locals {

  app_name       = "ccortez-app01"

  profile_prefix = "ccortez-env"

}



locals {

  profile = {

    "test" = "${local.profile_prefix}-test"

  }

  region = {

    "test"     = "us-east-1"

  }

}



locals {

  environment = "${terraform.workspace}"

}



locals {

  common_tags = {

    Terraform   = "true"

    Environment = local.environment

  }

  name_prefix = "${local.app_name}-${local.environment}"

}
